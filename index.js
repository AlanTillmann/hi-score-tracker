const express = require('express');

const app = express();
app.use(express.json());

const scores = [];

app.get('/scores', (req, res) =>
{
    res.status(500).send('Not implemented yet.');
});

app.post('/scores', (req, res) =>
{
    // {initials:"AAA", points:1000}
    const score = req.body;
    scores.push(score);
    res.send('Your score has been successfully added.');
});

app.listen(3000, ()=>console.log("Application started"));
